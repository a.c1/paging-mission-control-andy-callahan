### Overview
I completed the challenge _Paging Mission Control_ in Javascript with the Node.js runtime.

### Getting Started  
Clone this repository in a Node environment and install the dependencies using `npm install`.  
Dependencies used:
- Chai (Assertion Library)
- Mocha (Testing framework)

### Usage
Enter the project directory and run with `npm start`. The program with read the input file _givenTest.txt_ and print out an array of violations in JSON format. When three violations occur within a 5 minute interval, the first of the three is outputed.
### Testing
Run `npm test`. The testing framework will run the program with different input files and check if the output is as expected. Five test cases are included in the test directory.

### File Documentation
**index.js**: Entry point of the program. Reads an input file path, calls App.js, and prints the output.

**App.js**: Parses a text file line by line and handles any values that violate the limit.

**battHandler.js**: Handles battery violations by storing the number of occurences and timestamps in hashmaps. Each entry in the map tracks a different satellite. 

**tstatHandler.js**: Same functionallity as battHandler but handles thermostat violations.

**dataObject.js**: Holds a Javascript object that stores the attributes of an input line. 
