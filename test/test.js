import {assert} from 'chai';
import { processInput } from '../src/App.js';

describe('index', () => {
    it('given test case', async () => {
        const path = "src/givenTest.txt";
        const expected = [
            {satelliteId: 1000, severity: "RED HIGH", component: "TSTAT", timestamp: new Date("2018-01-01T23:01:38.001Z")},
            {satelliteId: 1000, severity: "RED LOW", component: "BATT", timestamp: new Date("2018-01-01T23:01:09.521Z")}
        ]
        const actual = await processInput(path)
        assert.deepEqual(actual, expected);
    });

    it('3 battery violations within 5 mins', async () => {
        const path = "test/testBatt.txt";
        const expected = [
            {satelliteId: 1000, severity: "RED LOW", component: "BATT", timestamp: new Date("2023-11-20T12:39:59.789Z")}
        ]
        const actual = await processInput(path)
        assert.deepEqual(actual, expected);
    });

    it('3 thermostat violations within 5 mins', async () => {
        const path = "test/testTstat.txt";
        const expected = [
            {satelliteId: 1000, severity: "RED HIGH", component: "TSTAT", timestamp: new Date("2023-11-20T13:34:58.789Z")}
        ]
        const actual = await processInput(path)
        assert.deepEqual(actual, expected);
    });

    it('3 batt violations outside 5 mins', async () => {
        const path = "test/testOutside5Min.txt";
        const expected = []
        const actual = await processInput(path)
        assert.deepEqual(actual, expected);
    });

    it('3 batt violations from 2 different sattelites', async () => {
        const path = "test/testMultipleSatt.txt";
        const expected = [
            {satelliteId: 1000, severity: "RED LOW", component: "BATT", timestamp: new Date("2023-11-20T12:34:57.789Z")},
            {satelliteId: 1001, severity: "RED LOW", component: "BATT", timestamp: new Date("2023-11-20T12:44:57.789Z")}
        ]
        const actual = await processInput(path)
        assert.deepEqual(actual, expected);
    });

    it('3 batt and tstat violations from 2 different sattelites', async () => {
        const path = "test/testBAndTMultSatt.txt";
        const expected = [
            {satelliteId: 1000, severity: "RED LOW", component: "BATT", timestamp: new Date("2023-11-20T12:34:57.789Z")},
            {satelliteId: 1001, severity: "RED LOW", component: "BATT", timestamp: new Date("2023-11-20T12:44:57.789Z")},
            {satelliteId: 1000, severity: "RED HIGH", component: "TSTAT", timestamp: new Date("2023-11-20T12:45:56.789Z")},
            {satelliteId: 1001, severity: "RED HIGH", component: "TSTAT", timestamp: new Date("2023-11-20T12:55:56.789Z")}
        ]
        const actual = await processInput(path)
        assert.deepEqual(actual, expected);
    });
});
