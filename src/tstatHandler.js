export function handleTSTAT(data, tstatCount, tstatTime, out){
    if (tstatCount.has(data.id)){
        const count = tstatCount.get(data.id);
        tstatCount.set(data.id, count + 1);
    }
    else{
        tstatCount.set(data.id, 1);
    }

    // save the time of the first violation within a 5 min interval
    if (tstatCount.get(data.id) === 1){
        tstatTime.set(data.id, data.timestamp);
    }
    // reset the violation count if subsequent violations exceeds the interval
    else if (tstatCount.get(data.id) > 1 && data.timestamp.getTime() - tstatTime.get(data.id).getTime() > 5 * 60 * 1000){
        tstatCount.set(data.id, 1)
        tstatTime.set(data.id, data.timestamp);
    }

    if (tstatCount.get(data.id) === 3){
        const errMsg = {
            satelliteId: data.id, 
            severity: "RED HIGH", 
            component: data.component, 
            timestamp: tstatTime.get(data.id)
        };
        out.push(errMsg);
    }
}