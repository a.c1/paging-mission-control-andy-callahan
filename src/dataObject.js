export function createDataObj(timestamp, id, redHigh, yellowHigh, yellowLow, redLow, rawValue, component){
    const timeFormat = `${timestamp.slice(0,4)}-${timestamp.slice(4,6)}-${timestamp.slice(6,8)}T${timestamp.slice(9)}Z`;
    return {
        timestamp: new Date(timeFormat),   
        id: parseInt(id),
        redHigh: parseFloat(redHigh),
        yellowHigh: parseFloat(yellowHigh),
        yellowLow: parseFloat(yellowLow),
        redLow: parseFloat(redLow),
        rawValue: parseFloat(rawValue),
        component: component
    }
}