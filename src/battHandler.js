export function handleBATT(data, battCount, battTime, out){
    if (battCount.has(data.id)){
        const count = battCount.get(data.id);
        battCount.set(data.id, count + 1);
    }
    else{
        battCount.set(data.id, 1);
    }

    // save the time of the first violation within a 5 min interval
    if (battCount.get(data.id) === 1){
        battTime.set(data.id, data.timestamp);
    }
    // reset the violation count if subsequent violations exceed the interval
    else if (battCount.get(data.id) > 1 && data.timestamp.getTime() - battTime.get(data.id).getTime() > 5 * 60 * 1000){
        battCount.set(data.id, 1);
        battTime.set(data.id, data.timestamp);
    }

    if (battCount.get(data.id) === 3){
        const errMsg = {
            satelliteId: data.id, 
            severity: "RED LOW", 
            component: data.component, 
            timestamp: battTime.get(data.id)
        };
        out.push(errMsg);
    }
}