import { processInput } from "./App.js";

const filePath = "./src/givenTest.txt";

const output = await processInput(filePath);
console.log(JSON.stringify(output, null, 4));
