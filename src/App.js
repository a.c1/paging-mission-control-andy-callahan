import { createReadStream } from "fs";
import { createInterface } from "readline";
import { createDataObj } from "./dataObject.js";
import { handleBATT } from "./battHandler.js";
import { handleTSTAT } from "./tstatHandler.js";

export function processInput(filePath){
    const out = [];
    // track number of limit violations and the first violation time for each satellite
    const battCount = new Map();
    const tstatCount = new Map();
    const battTime = new Map();
    const tstatTime = new Map();

    return new Promise((resolve) => {
        const rl = createInterface({
            input: createReadStream(filePath),
            crlfDelay: Infinity
        });
        
        rl.on('line', (line) => {
            const values = line.split("|");
            const data = createDataObj(...values);
        
            if (data.component === "BATT" && data.rawValue < data.redLow){
                handleBATT(data, battCount, battTime, out);
            }
            else if (data.component === "TSTAT" && data.rawValue > data.redHigh){
                handleTSTAT(data, tstatCount, tstatTime, out);
            }
        });
    
        rl.on('close', () => {
            resolve(out);
        });    
    });
}


